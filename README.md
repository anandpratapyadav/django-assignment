NOTE: This set is based on Pythin Django framework. So Basic requirements should be installed in system for python project.
 Here is the database properties for mysql database:

    'DB NAME': 'djangodatabase',
    'USER': 'root',
    'PASSWORD': 'root',
    'HOST': 'localhost',
    'PORT': '3306',

Steps to run API in local set up.
1). Clone this repository.
2). Create virtual environment. Command `python3 -m venv env`.
3). Run envirnmemnt . Command `env\Scripts\activate`.
4). In the same environment install all lib requirements. Command `pip install -r requirements.txt`.
5). Then run command to create table. `python manage.py migrate`. then seed data in table.
6). Here is the command to run application `python manage.py runserver`.

Then finally you need to call API as `http://127.0.0.1:8000/datasets/`. This api call is already there in UI environment file.
