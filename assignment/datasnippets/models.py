from django.db import models


# Create your models here.

class DataSnippets(models.Model):
    city = models.CharField(max_length=50, blank=True, default='')
    start_date = models.CharField(max_length=50, blank=True, default='')
    end_date = models.CharField(max_length=50, blank=True, default='')
    price = models.DecimalField(max_digits=5, decimal_places=2)
    status = models.CharField(max_length=50, blank=True, default='')
    color = models.CharField(max_length=10, blank=True, default='')

    class Meta:
        ordering = ['id']
