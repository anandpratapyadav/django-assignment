from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from datasnippets.models import DataSnippets
from datasnippets.serializers import DataSnippetsSerializer

@csrf_exempt
def get_data_snippet_list(request):
    """
    List All Data
    """
    if request.method == 'GET':
        dataSnippets = DataSnippets.objects.all()
        serializer = DataSnippetsSerializer(dataSnippets, many=True)
        return JsonResponse(serializer.data, safe=False)