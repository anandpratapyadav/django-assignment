from rest_framework import serializers
from datasnippets.models import DataSnippets

class DataSnippetsSerializer(serializers.ModelSerializer):
    class Meta:
        model = DataSnippets
        fields = ['id', 'city', 'start_date', 'end_date', 'price', 'status', 'color']
