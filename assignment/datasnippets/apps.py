from django.apps import AppConfig


class DatasnippetsConfig(AppConfig):
    name = 'datasnippets'
